/*
Criar a tabela de usuário "tb_usuario"
MySQL - 5.6.25 : Database - dbunip01_pro
*********************************************************************
*/
CREATE TABLE `tb_usuario` (
  `usua_ID` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `usua_RA` varchar(30) DEFAULT NULL,
  `usua_Nome` varchar(50) DEFAULT NULL,
  `usua_Senha` varchar(50) DEFAULT NULL,
  `usua_email` varchar(100) DEFAULT NULL,
  `usua_ativo` varchar(5) DEFAULT 'off',
  `usua_dt_login` datetime DEFAULT NULL,
  `usua_tokenAtv` varchar(50) DEFAULT NULL,
  `usua_cNivel` int(4) DEFAULT NULL,
  `usua_cCurso` int(4) DEFAULT NULL,
  `usua_cTurno` int(4) DEFAULT NULL,
  PRIMARY KEY (`usua_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1
