<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Mostrar e fechar div com jQuery</title>
<script src="../public/js/jquery.js" type="text/javascript"></script>
</head>
<body>
<style type="text/css" media="all"> 
#divMostra {
	width:300px; 
	height:300px;
	margin:10px 0;
	border:2px solid #000;
	color:#fff;
	background:#f00; 
	display:none;
	}
</style>

<script type="text/javascript"> 
	
	$(document).ready(function(){
	      $('#mostra').click(function(){
			//mostra div
		  	$('#divMostra').show();
		  		
	      });
	      $('#fecha').click(function(){
			//oculta div
		  	$('#divMostra').hide();
		  		
	      });
	 });
     
</script>

<a href="javascript: void(0);" id="mostra">Mostrar</a>&nbsp;<a href="javascript: void(0);" id="fecha">Fechar</a> 

<div id="divMostra">Mostra div com jQuery</div>

</body>
</html>